<?php

namespace App\Tests;

use App\Entity\ContractType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class ContractTypeTest extends TestCase
{
    public function testAddContractType(): void
    {
        $contractType = new ContractType();
        $contractType->setLabel("Contrat Cdd1");
        $entityManager = $this->createMock(EntityManager::class);

        $entityManager
            ->expects($this->once())
            ->method('persist')
            ->with($this->isInstanceOf(ContractType::class));

        $entityManager
            ->expects($this->once())
            ->method('flush');

        $this->assertEquals($contractType->getLabel(), "Contrat Cdd1");
    }
}
