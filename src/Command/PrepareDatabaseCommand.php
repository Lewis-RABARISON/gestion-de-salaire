<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
name:
'app:prepare-database',
    description: 'Prepare la base de donnée',
)]
class PrepareDatabaseCommand extends Command
{
    /**
     * Execute la commande de preparation de la base de donnée
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->section("Suppression de la base de donnée,creation de la base de donnée,creation de fausse donnée");
        $this->runSymfonyCommand($input, $output, 'doctrine:database:drop', true);
        $this->runSymfonyCommand($input, $output, 'doctrine:database:create');
        $this->runSymfonyCommand($input, $output, 'doctrine:migrations:migrate');
        $this->runSymfonyCommand($input, $output, 'doctrine:fixtures:load');
        $io->success("La base de donnée est crée et il y a des données disponible");

        return Command::SUCCESS;
    }

    /**
     * Execute les commandes symfony
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param string $command
     * @param bool $forceOption
     * @throws \Exception
     */
    public function runSymfonyCommand(InputInterface $input, OutputInterface $output,
                                      string $command, bool $forceOption = false): void
    {
        $application = $this->getApplication();
        if (!$application) {
            throw new \LogicException("No application");
        }
        $command = $application->find($command);
        if ($forceOption) {
            $input = new ArrayInput([
                '--force' => true
            ]);
        }
        $input->setInteractive(false);
        $command->run($input, $output);
    }
}
