<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Wage;
use App\Entity\WageCsvFile;
use App\Repository\UserRepository;
use App\Repository\WageCsvFileRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

#[AsCommand(
    name: 'app:import-data-wage-csv',
    description: 'Importe le fichier csv',
)]
class ImportDataWageCsvCommand extends Command
{
    /**
     * Constructeur ImportDataWageCsvCommand.
     * @param ParameterBagInterface $parameterBag
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $entityManager)
     */
    public function __construct(private ParameterBagInterface $parameterBag,
                                private UserRepository $userRepository,
                                private EntityManagerInterface $entityManager,
                                private WageCsvFileRepository $wageCsvFileRepository
                                )
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('csvFilename', InputArgument::OPTIONAL, 'Nom du fichier csv')
        ;
    }

    /**
     * Execute la commande d'importation du fichier de salaires en csv
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $csvFile = $input->getArgument('csvFilename');
        $io = new SymfonyStyle($input, $output);
        $io->section('Importe la Liste des salaires.');

        $file = $this->parameterBag->get('wage_data') . '/' . $csvFile;
        $format = pathinfo($file, PATHINFO_EXTENSION);

        $normalizer = [new ObjectNormalizer()];
        $encoder = [new CsvEncoder()];
        $serializer = new Serializer($normalizer, $encoder);

        $data = file_get_contents($file);
        $dataImported = $serializer->decode($data, $format);

        $this->saveData($dataImported);
        $this->updateStatusCsvFile($csvFile);

        $io->success('Liste des salaires importé avec succès.');

        return Command::SUCCESS;
    }

    /**
     * Enregistre les données
     * @param $dataImported
     */
    public function saveData($dataImported)
    {
        foreach ($dataImported as $item) {
            $wages = new Wage();
            $register = $item['Matricule'];
            $employee = $this->userRepository->findOneBy(['matricule' => $item['Matricule']]);
            $wages->setEmployee($employee);
            $wages->setReceptionDate(new \DateTime());
            $wages->setAmount((float) $item['Dernier salaire reçu']);
            $this->entityManager->persist($wages);
        }

        $this->entityManager->flush();
    }

    public function updateStatusCsvFile($file)
    {
        /** @var WageCsvFile $wageCsvFile */
        $wageCsvFile = $this->wageCsvFileRepository->findOneBy(compact('file'));
        $wageCsvFile->setIsAlreadyImported(true);
        $this->entityManager->flush();
    }
}
