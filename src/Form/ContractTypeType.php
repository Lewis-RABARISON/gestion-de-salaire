<?php

namespace App\Form;

use App\Entity\ContractType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContractTypeType extends AbstractType
{
    /**
     * Construit le formulaire du type de contrat
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('label', TextType::class, [
                'label' => 'Type de contrat',
                'empty_data' => ''
            ])
        ;
    }

    /**
     * Lie la donnée du formulaire avec l'entité
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ContractType::class,
        ]);
    }
}
