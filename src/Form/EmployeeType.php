<?php

namespace App\Form;

use App\Entity\ContractType;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class EmployeeType extends AbstractType
{
    /**
     * Construit le formulaire du salarié
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('matricule', TextType::class, [

            ])
            ->add('lastname', TextType::class, [
                'label' => 'Nom de famille',

            ])
            ->add('firstname', TextType::class, [
                'label' => 'Prénom'
            ])
            ->add('email', EmailType::class, [

                'attr' => [
                    'autocomplete' => 'off'
                ]
            ])
            ->add('dateOfBirthAt', BirthdayType::class, [
                'label' => 'Date de naissance',
                'widget' => 'single_text',
            ])
            ->add('phoneNumber', TelType::class, [
                'label' => 'Téléphone',
            ])
            ->add('photoToUpload', FileType::class,[
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Veuillez uploader seulement un image jpeg ou jpg ou png'
                    ])
                ]
            ])
            ->add('wageAmount', IntegerType::class, [
                'label' => 'Montant du salaire'
            ])
            ->add('contractType', EntityType::class, [
                'label' => 'Type de contrat',
                'class' => ContractType::class,
                'choice_label' => 'label',
                'placeholder' => 'Séléctionner un type de contrat'
            ])
        ;
    }

    /**
     * Lie la donnée du formulaire avec l'entité
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
