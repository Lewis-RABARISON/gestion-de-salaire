<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\ContractType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ContractTypeRepository extends ServiceEntityRepository
{
    /**
     * Constructeur ContractTypeRepository.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContractType::class);
    }

    /**
     * Permet d'hydrater les données et les sauvegarder dans la base de donnée
     * @param ContractType $entity
     * @param bool $flush
     */
    public function add(ContractType $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Permet de supprimer les données et met à jour la base de donnée
     * @param ContractType $entity
     * @param bool $flush
     */
    public function remove(ContractType $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Recupère la liste des types de contrats sans doublon
     * @return array
     */
    public function findAllContractTypeEmployees(): array
    {
        return $this->_em->createQuery('SELECT DISTINCT (c.label) AS label FROM App\Entity\ContractType c')
            ->getResult();
    }
}
