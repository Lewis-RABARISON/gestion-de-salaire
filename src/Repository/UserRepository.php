<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\ContractType;
use App\Entity\User;
use App\Utils\Globals;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

class UserRepository extends ServiceEntityRepository
{
    /**
     * Constructeur UserRepository.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Permet d'hydrater les données et les sauvegarder dans la base de donnée
     * @param User $entity
     * @param bool $flush
     */
    public function add(User $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Permet de supprimer les données et met à jour la base de donnée
     * @param User $entity
     * @param bool $flush
     */
    public function remove(User $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Compte les salariés
     * @return int
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countEmployees(): int
    {
        $result = $this->createQueryBuilder('u')
            ->select('COUNT(u)')
            ->andWhere('u.roles LIKE :role')
            ->setParameter('role', '%'.Globals::ROLE_EMPLOYEE.'%')
            ->getQuery()
            ->getSingleResult();

        return $result[1] != 0 ? $result[1] : 0;
    }

    /**
     * Recupère la liste des salariés
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function findAllEmployees(int $page, int $limit): array
    {
        return $this->createQueryBuilder('u')
            ->select('u')
            ->orderBy('u.id','DESC')
            ->andWhere('u.roles NOT LIKE :role')
            ->setParameter('role', '%'.Globals::ROLE_ADMIN.'%')
            ->setMaxResults($limit)
            ->setFirstResult(($page * $limit) - $limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * Recupère la liste des salariés sans pagination
     * @return array
     */
    public function findAllEmployeesNotPaginated(): array
    {
        return $this->createQueryBuilder('u')
            ->select('u')
            ->orderBy('u.id','DESC')
            ->andWhere('u.roles LIKE :role')
            ->setParameter('role', '%'.Globals::ROLE_EMPLOYEE.'%')
            ->getQuery()
            ->getResult();
    }

    /**
     * Filtre les salariés en fonction de son matricule, nom, prénom et type de contrat
     * @param array $data
     * @return array
     */
    public function filterEmployees(array $data): array
    {
        $qb = $this->createQueryBuilder('u')
            ->select('u')
            ->orderBy('u.id','DESC')
            ->andWhere('u.roles LIKE :role')
            ->setParameter('role', '%'.Globals::ROLE_EMPLOYEE.'%')
            ->setMaxResults(3);

        if (isset($data['register_filter']) && $data['register_filter'] != "Matricule") {
            $qb->andWhere('u.matricule LIKE :register')
                ->setParameter('register', $data['register_filter']);
        }

        if (isset($data['lastname_filter']) && $data['lastname_filter'] != "Nom") {
            $qb->andWhere('u.lastname LIKE :lastname')
                ->setParameter('lastname', $data['lastname_filter']);
        }

        if (isset($data['firstname_filter']) && $data['firstname_filter'] != "Prénom") {
            $qb->andWhere('u.firstname LIKE :firstname')
                ->setParameter('firstname', $data['firstname_filter']);
        }

        if (isset($data['contract_type_filter']) && $data['contract_type_filter'] != "Type de contrat") {
            $qb->andWhere('c.label LIKE :contractTypeLabel')
                ->leftJoin('u.contractType','c')
                ->setParameter('contractTypeLabel', $data['contract_type_filter']);
        }

        return $qb
            ->getQuery()
            ->getResult();
    }

    /**
     * Liste des salaires du salarié
     * @param string $register
     * @return array
     */
    public function allWages(string $register): array
    {
        $qb = $this->createQueryBuilder('u')
            ->select('u')
            ->andWhere('u.matricule = :register')
            ->setParameter('register', $register);

        return $qb
            ->getQuery()
            ->getResult();
    }
}
