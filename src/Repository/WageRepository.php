<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Wage;
use App\Utils\Globals;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class WageRepository extends ServiceEntityRepository
{
    /**
     * Constructeur WageRepository.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Wage::class);
    }

    /**
     * Liste des années sans doublon dans la liste des salaires
     * @return array
     */
    public function getAllYearWage(): array
    {
        return $this->_em->createQuery('SELECT DISTINCT(YEAR(w.receptionDate)) AS year FROM App\Entity\Wage w')
            ->getResult();
    }

    /**
     * Liste des mois sans doublon dans la liste des salaires
     * @return array
     */
    public function getAllMonthWage(): array
    {
        return $this->_em->createQuery('SELECT DISTINCT(MONTH(w.receptionDate)) AS month FROM App\Entity\Wage w')
            ->getResult();
    }

    /**
     * Filtre les salariés en fonction du mois, l'année et le salarié
     * @param array $data
     * @return array
     */
    public function filterWages(array $data): array
    {
        $qb = $this->createQueryBuilder('w')
            ->select('w')
            ->leftJoin('w.employee', 'e')
            ->orderBy('w.id', 'DESC')
            ->andWhere('e.roles LIKE :role')
            ->setParameter('role', '%' . Globals::ROLE_EMPLOYEE . '%');

        if (isset($data['employee_wage_filter']) && $data['employee_wage_filter'] != "Salarié") {
            $qb->andWhere('e.matricule = :register')
                ->setParameter('register', $data['employee_wage_filter']);
        }

        if (isset($data['year_wage_filter']) && $data['year_wage_filter'] != "Année") {
            $qb->andWhere("YEAR(w.receptionDate) = :year")
                ->setParameter('year', $data['year_wage_filter']);
        }

        if (isset($data['month_wage_filter']) && $data['month_wage_filter'] != "Mois") {
            $qb->andWhere("MONTH(w.receptionDate) LIKE :month")
                ->setParameter('month', $data['month_wage_filter']);
        }

        return $qb->getQuery()->getResult();
    }
}
