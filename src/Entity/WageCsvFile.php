<?php

namespace App\Entity;

use App\Repository\WageCsvFileRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: WageCsvFileRepository::class)]
class WageCsvFile
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $file;

    #[ORM\Column(type: 'datetime_immutable')]
    private $generatedDateAt;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private $isAlreadyImported;

    public function __construct()
    {
        $this->generatedDateAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getGeneratedDateAt(): ?\DateTimeImmutable
    {
        return $this->generatedDateAt;
    }

    public function setGeneratedDateAt(\DateTimeImmutable $generatedDateAt): self
    {
        $this->generatedDateAt = $generatedDateAt;

        return $this;
    }

    public function isIsAlreadyImported(): ?bool
    {
        return $this->isAlreadyImported;
    }

    public function setIsAlreadyImported(?bool $isAlreadyImported): self
    {
        $this->isAlreadyImported = $isAlreadyImported;

        return $this;
    }
}
