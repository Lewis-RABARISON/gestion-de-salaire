<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\WageRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\User;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: WageRepository::class)]
class Wage
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'wages')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups("employee_read")]
    private ?User $employee;

    #[ORM\Column(type: 'datetime')]
    #[Groups("employee_read")]
    private ?\DateTimeInterface $receptionDate;

    #[ORM\Column(type: 'float')]
    #[Groups("employee_read")]
    private float $amount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmployee(): ?User
    {
        return $this->employee;
    }

    public function setEmployee(?User $employee): self
    {
        $this->employee = $employee;

        return $this;
    }

    public function getReceptionDate(): ?\DateTimeInterface
    {
        return $this->receptionDate;
    }

    public function setReceptionDate(\DateTimeInterface $receptionDate): self
    {
        $this->receptionDate = $receptionDate;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }
}
