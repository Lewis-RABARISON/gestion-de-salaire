<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\UserRepository;
use App\Entity\ContractType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity('email', message: "Cet email appartient déjà à quelqu'un")]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', length: 180, unique: true, nullable: true)]
    #[Assert\NotBlank(message: "L'email est obligatoire")]
    #[Assert\Email(message: "Veuillez renseigner une addresse email valide")]
    #[Assert\Length(max: 180, maxMessage: "L'addresse email ne doit pas depasser les 180 caractères")]
    private string $email;

    #[ORM\Column(type: 'json')]
    private array $roles = [];

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank(message: "Le mot de passe est obligatoire")]
    private string $password;

    #[ORM\Column(type: 'string', length: 15)]
    #[Assert\NotBlank(message: "Le matricule est obligatoire")]
    #[Groups("employee_read")]
    private string $matricule;

    #[ORM\Column(type: 'string', length: 70, nullable: true)]
    #[Assert\Length(max: 70, maxMessage: "Le prénom ne doit pas depasser les 180 caractères")]
    #[Groups("employee_read")]
    private ?string $firstname;

    #[ORM\Column(type: 'string', length: 60)]
    #[Assert\NotBlank(message: "Le nom de famille est obligatoire")]
    #[Assert\Length(max: 60, maxMessage: "Le nom de famille ne doit pas depasser les 180 caractères")]
    #[Groups("employee_read")]
    private string $lastname;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups("employee_read")]
    private ?\DateTimeImmutable $hiringDateAt;

    #[ORM\Column(type: 'datetime')]
    #[Assert\NotBlank(message: "La date de naissance est obligatoire")]
    #[Groups("employee_read")]
    private \DateTimeInterface $dateOfBirthAt;

    #[ORM\Column(type: 'string', length: 20, nullable: true)]
    #[Groups("employee_read")]
    private ?string $phoneNumber;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $photo;

    #[ORM\Column(type: 'float', nullable: true)]
    #[Groups("employee_read")]
    private ?float $wageAmount;

    #[ORM\OneToMany(mappedBy: 'employee', targetEntity: Wage::class)]
    private Collection $wages;

    #[ORM\ManyToOne(targetEntity: ContractType::class, inversedBy: 'employees', cascade: ["persist"])]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups("employee_read", "contract_type_read")]
    private ContractType $contractType;

    public function __construct()
    {
        $this->wages = new ArrayCollection();
        $this->hiringDateAt = new \DateTimeImmutable();
        $this->wageAmount = 0;
    }

    public function fullname(): string
    {
        return $this->lastname . " " . $this->firstname;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection<int, Wage>
     */
    public function getWages(): Collection
    {
        return $this->wages;
    }

    public function addWage(Wage $wage): self
    {
        if (!$this->wages->contains($wage)) {
            $this->wages[] = $wage;
            $wage->setEmployee($this);
        }

        return $this;
    }

    public function removeWage(Wage $wage): self
    {
        if ($this->wages->removeElement($wage)) {
            // set the owning side to null (unless already changed)
            if ($wage->getEmployee() === $this) {
                $wage->setEmployee(null);
            }
        }

        return $this;
    }

    public function getMatricule(): ?string
    {
        return $this->matricule;
    }

    public function setMatricule(string $matricule): self
    {
        $this->matricule = $matricule;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getHiringDateAt(): ?\DateTimeImmutable
    {
        return $this->hiringDateAt;
    }

    public function setHiringDateAt(\DateTimeImmutable $hiringDateAt): self
    {
        $this->hiringDateAt = $hiringDateAt;

        return $this;
    }

    public function getDateOfBirthAt(): \DateTimeInterface
    {
        return $this->dateOfBirthAt;
    }

    public function setDateOfBirthAt($dateOfBirthAt): self
    {
        $this->dateOfBirthAt = $dateOfBirthAt;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }
    
    public function getWageAmount(): ?float
    {
        return $this->wageAmount;
    }

    public function setWageAmount(?float $wageAmount): self
    {
        $this->wageAmount = $wageAmount;

        return $this;
    }

    public function getContractType(): ?ContractType
    {
        return $this->contractType;
    }

    public function setContractType(?ContractType $contractType): self
    {
        $this->contractType = $contractType;

        return $this;
    }
}
