<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\WageCsvFile;
use App\Repository\WageCsvFileRepository;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\String\Slugger\SluggerInterface;

class ExportCsvWageService
{
    public function __construct( private ParameterBagInterface $parameterBag,
                                private SluggerInterface $slugger,
                                private WageCsvFileRepository $wageCsvFileRepository)
    {
    }

    /**
     * Reponse csv
     * @param array $employeeslist
     * @return Response
     */
    public function responseCsv(array $employeeslist): Response
    {
        $now = (new \DateTime())->format("d-m-Y H:i:s");
        $filename = $this->slugger->slug("salaire-$now") . ".csv";
        $fp = fopen('php://temp', 'w');
        fputs($fp, "\xEF\xBB\xBF");

        foreach ($employeeslist as $employeeItem) {
            fputcsv($fp, $employeeItem);
        }

        rewind($fp);
        $response = new Response(stream_get_contents($fp));
        fclose($fp);
        $response->headers->set('Content-Type', 'text/csv; charset=UTF-8');
        $response->headers->set('Content-Disposition', "attachment; filename=$filename");

        $fileSystem = new Filesystem();
        $exportedFile = "{$this->parameterBag->get('wage_data')}/$filename";

        try {
            $fileSystem->touch($filename);
            file_put_contents($filename, $response->getContent());
            rename($filename, $exportedFile);
        } catch (IOException $error){
            throw new IOException($error);
        }

        $wageCsvFile = new WageCsvFile();
        $wageCsvFile->setFile($filename);
        $this->wageCsvFileRepository->add($wageCsvFile, true);

        return $response;
    }
}