<?php

namespace App\Controller;

use App\Entity\ContractType;
use App\Entity\Wage;
use App\Repository\ContractTypeRepository;
use App\Repository\UserRepository;
use App\Repository\WageRepository;
use App\Service\UploadPhotoEmployeeService;
use App\Twig\FormatMonthExtension;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use App\Service\ExportCsvWageService;

class AdminWageController extends AbstractController
{
    /**
     * Constructeur AdminWageController.
     * @param WageRepository $wageRepository
     * @param UserRepository $employeeRepository
     * @param ContractTypeRepository $contractTypeRepository
     * @param ExportCsvWageService $exportCsvWageService
     */
    public function __construct(private WageRepository $wageRepository,
                                private UserRepository $employeeRepository,
                                private ContractTypeRepository $contractTypeRepository,
                                private ExportCsvWageService $exportCsvWageService)
    {
    }

    /**
     * Affiche la liste des salaires
     * @return Response
     */
    #[Route('/admin/salaire', name: 'admin_wage_index', methods: ['GET'])]
    public function index(): Response
    {
        $wages = $this->wageRepository->findBy([], ['id' => 'DESC']);
        $yearWages = $this->wageRepository->getAllYearWage();
        $monthWages = $this->wageRepository->getAllMonthWage();
        $employeesNotPaginated = $this->employeeRepository->findAllEmployeesNotPaginated();

        return $this->render('backoffice/wage/index.html.twig',
            compact('wages', 'yearWages', 'monthWages', 'employeesNotPaginated'));
    }

    /**
     * Filtre la liste des salaires en fonction du mois, de l'année et le salarié
     * @param Request $request
     * @return JsonResponse
     */
    #[Route('/admin/salaire/filtre-salaire', name: 'filter_wage' )]
    public function sortEmployee(Request $request): JsonResponse
    {
        $response = [];

        if ($request->isXmlHttpRequest()) {
            $data = $request->request->all();
            $wages = $this->wageRepository->filterWages($data);
            $response = [
                'success' => true,
                'wages' => $this->renderView("backoffice/wage/_list_wage.html.twig", compact('wages')),
            ];
        }

        return $this->json($response);
    }

    /**
     * Exporte la liste des salaires du mois
     * @return Response
     */
    #[Route('/admin/salaire/exporter', name: 'export_csv_wage', methods: ['GET', 'POST'])]
    public function exportCsv(): Response
    {
        $employeeslist = [];
        $employeeslist[] = ['Nom', 'Prénom', 'Matricule', "Date d'embauche", 'Date de naissance',
            'Téléphone', 'Type de contrat', 'Dernier salaire reçu', 'Mois du dernier salaire reçu'];

        $employeesNotPaginated = $this->employeeRepository->findAllEmployeesNotPaginated();
        foreach ($employeesNotPaginated as $key => $employeeNotPaginated) {
            if ($employeesNotPaginated[$key]->getContractType()->getId()) {
                /** @var ContractType $contractType */
                $contractType = $this->contractTypeRepository->find($employeesNotPaginated[$key]->getContractType()->getId());
                $contractTypeLabel = $contractType->getLabel();

                $employeeslist[] = [
                    $employeesNotPaginated[$key]->getLastname(), $employeesNotPaginated[$key]->getFirstname(),
                    $employeesNotPaginated[$key]->getMatricule(),
                    $employeesNotPaginated[$key]->getHiringDateAt()->format('d-m-Y'),
                    $employeesNotPaginated[$key]->getDateOfBirthAt()->format('d-m-Y'),
                    $employeesNotPaginated[$key]->getPhoneNumber(),
                    $contractTypeLabel, $employeesNotPaginated[$key]->getWageAmount(),
                    FormatMonthExtension::formatMonth((new \DateTime())->format('M'))
                ];
            } else {
                echo "Il n'y a pas type de contrat";
            }
        }

        return $this->exportCsvWageService->responseCsv($employeeslist);
    }

    /**
     * Api en json de la liste des salaires
     * @return Response
     */
    #[Route('/api/salaires/', name: 'export_json_wage', methods: ['GET', 'POST'])]
    public function exportJson(): Response
    {
        $response = [];
        $now = (new \DateTime())->format("d-m-Y H:i:s");
        $wageslist = [];
        $wages = $this->wageRepository->findBy([], ['id' => 'DESC']);

        return $this->json($wages, Response::HTTP_OK, ["Application/json"], ['groups' => "employee_read"]);
    }
}
