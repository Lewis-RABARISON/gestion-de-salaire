<?php

namespace App\Controller;

use App\Entity\WageCsvFile;
use App\Repository\WageCsvFileRepository;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;


class AdminImportController extends AbstractController
{
    /**
     * @var WageCsvFileRepository
     */
    private $wageCsvFileRepository;
    /**
     * @var KernelInterface
     */
    private $kernel;

    public function __construct(WageCsvFileRepository $wageCsvFileRepository,
                                KernelInterface $kernel)
    {
        $this->wageCsvFileRepository = $wageCsvFileRepository;
        $this->kernel = $kernel;
    }

    #[Route('/admin/importation-salaires', name: 'admin_import_wage_list')]
    public function index(): Response
    {
        $wageCsvFiles = $this->wageCsvFileRepository->findBy([], ['generatedDateAt' => 'DESC']);

        return $this->render('backoffice/import/index.html.twig', compact('wageCsvFiles'));
    }

    #[Route('/admin/import-salaire/{id}', name: 'admin_import_wage')]
    public function import(WageCsvFile $wageCsvFile): Response
    {
        $application = new Application($this->kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput([
            'command' => 'app:import-data-wage-csv',
            'csvFilename' => $wageCsvFile->getFile()
        ]);

        $output = new BufferedOutput();

        try {
            $application->run($input,$output);
        } catch (\Exception $error){
            throw new \Exception($error);
        }

        return $this->redirectToRoute('admin_import_wage_list');
    }
}
