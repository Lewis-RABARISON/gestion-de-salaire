<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\ContractTypeRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminDashboardController extends AbstractController
{
    /**
     * Constructeur AdminContractTypeController.
     * @param ContractTypeRepository $contractTypeRepository
     */
    public function __construct(private ContractTypeRepository $contractTypeRepository,
                                private UserRepository $employeeRepository)
    {

    }

    /**
     * Affiche le tableau de bord
     * @return Response
     */
    #[Route('/admin/tableau-de-bord', name: 'admin_dashboard_index')]
    public function index(): Response
    {
        $count_contracts_types = $this->contractTypeRepository->count([]);
        $count_employees = $this->employeeRepository->countEmployees();

        return $this->render('backoffice/dashboard/index.html.twig', compact('count_contracts_types', 'count_employees'));
    }
}
