<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\ContractType;
use App\Form\ContractTypeType;
use App\Repository\ContractTypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/type-contrat')]
class AdminContractTypeController extends AbstractController
{
    /**
     * Constructeur AdminContractTypeController.
     * @param ContractTypeRepository $contractTypeRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(private ContractTypeRepository $contractTypeRepository,
                                private EntityManagerInterface $entityManager)
    {

    }

    /**
     * Affiche la liste des types de contrats
     * @return Response
     */
    #[Route('/', name: 'admin_contract_type_index', methods: ['GET'])]
    public function index(): Response
    {
        $contractTypes = $this->contractTypeRepository->findBy([], ['id' => 'DESC']);

        return $this->render('backoffice/contract_type/index.html.twig', compact('contractTypes'));
    }

    /**
     * Enregistre un type de contrat
     * @param Request $request
     * @return Response
     */
    #[Route('/ajout', name: 'admin_contract_type_add', methods: ['GET', 'POST'])]
    public function add(Request $request): Response
    {
        $contractType = new ContractType();
        $form = $this->createForm(ContractTypeType::class, $contractType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->contractTypeRepository->add($contractType, true);

            return $this->redirectToRoute('admin_contract_type_index');
        }

        return $this->renderForm('backoffice/contract_type/add.html.twig', compact( 'form'));
    }

    /**
     * Edite et permet de modifier le type de contrat
     * @param Request $request
     * @param ContractType $contractType
     * @return Response
     */
    #[Route('/{id}/editer', name: 'admin_contract_type_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ContractType $contractType): Response
    {
        $form = $this->createForm(ContractTypeType::class, $contractType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('admin_contract_type_index');
        }

        return $this->renderForm('backoffice/contract_type/edit.html.twig', compact('form'));
    }

    /**
     * Supprime un type de contrat
     * @param Request $request
     * @param ContractType $contractType
     * @param ContractTypeRepository $contractTypeRepository
     * @return Response
     */
    #[Route('/{id}/supprimer', name: 'admin_contract_type_delete', methods: ['POST'])]
    public function delete(Request $request, ContractType $contractType, ContractTypeRepository $contractTypeRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$contractType->getId(), $request->request->get('_token'))) {
            $contractTypeRepository->remove($contractType, true);
        }

        return $this->redirectToRoute('admin_contract_type_index', [], Response::HTTP_SEE_OTHER);
    }
}
