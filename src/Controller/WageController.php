<?php

namespace App\Controller;

use App\Entity\Wage;
use App\Repository\WageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/salaires')]
class WageController extends AbstractController
{
    /**
     * @var WageRepository
     */
    private $wageRepository;

    public function __construct(WageRepository $wageRepository)
    {
        $this->wageRepository = $wageRepository;
    }

    #[Route('/', name: 'wage_index')]
    public function index(): Response
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_login');
        }

        $wages =  $this->wageRepository->findBy(['employee' => $this->getUser()]);

        return $this->render('frontoffice/wage/index.html.twig', compact('wages'));
    }

    #[Route('/{id}/consultation', name: 'wage_show')]
    public function show(Wage $wage): Response
    {
        return $this->render('frontoffice/wage/show.html.twig', compact('wage'));
    }
}
