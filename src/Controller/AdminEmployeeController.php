<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Entity\Wage;
use App\Form\EmployeeType;
use App\Repository\ContractTypeRepository;
use App\Repository\UserRepository;
use App\Service\UploadPhotoEmployeeService;
use App\Utils\Globals;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

#[Route('/admin/salarie')]
class AdminEmployeeController extends AbstractController
{
    /**
     * Constructeur AdminEmployeeController.
     * @param UserRepository $employeeRepository
     * @param EntityManagerInterface $entityManager
     * @param UploadPhotoEmployeeService $uploadPhotoEmployee
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param ContractTypeRepository $contractTypeRepository)
     */
    public function __construct(private UserRepository $employeeRepository,
                                private EntityManagerInterface $entityManager,
                                private UploadPhotoEmployeeService $uploadPhotoEmployee,
                                private UserPasswordHasherInterface $userPasswordHasher,
                                private ContractTypeRepository $contractTypeRepository)
    {

    }

    /**
     * Affiche la liste des salariés
     * @param Request $request
     * @return Response
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    #[Route('/', name: 'admin_employee_index', options: ['expose' => true])]
    public function index(Request $request): Response
    {
        $employeesNotPaginated = $this->employeeRepository->findAllEmployeesNotPaginated();
        $contractTypes = $this->contractTypeRepository->findAllContractTypeEmployees();
        $page = 1;
        $limit = 3;
        $employees = $this->employeeRepository->findAllEmployees($page, $limit);
        $totalEmployee = $this->employeeRepository->countEmployees();
        $totalPages = ceil($totalEmployee / $limit);

        if ($request->isXmlHttpRequest()) {
            $response = [];

            // Pagination
            if ($request->request->get('page') != 0) {
                $page = (int) $request->request->get('page');
                $offset = ($page - 1) * $limit;
                $employees = $this->employeeRepository->findBy([], ['id' => 'DESC'], $limit, $offset);

                $response = [
                    'success' => true,
                    'employees' => $this->renderView("backoffice/employee/_list_employee.html.twig",
                                    compact('employees', 'totalPages', 'page')),
                    'pagination' => $this->renderView("partials/_pagination.html.twig", compact('totalPages', 'page'))
                ];
            }

            return $this->json($response);
        }

        return $this->renderForm('backoffice/employee/index.html.twig',
                                    compact('employees', 'totalPages', 'page', 'employeesNotPaginated',
                                        'contractTypes'));
    }

    /**
     * Filtre les salariés en fonction du matricule, nom, prenom et type de contrat
     * @param Request $request
     * @return JsonResponse
     */
    #[Route('/filtre-salarie', name: 'filter_employee' )]
    public function sortEmployee(Request $request): JsonResponse {
        $response = [];
        $page = 1;
        $limit = 3;
        $totalEmployee = $this->employeeRepository->countEmployees();
        $totalPages = ceil($totalEmployee / $limit);

        if ($request->isXmlHttpRequest()) {
            $data = $request->request->all();
            $employees = $this->employeeRepository->filterEmployees($data);
            $response = [
                'success' => true,
                'employees' => $this->renderView("backoffice/employee/_list_employee.html.twig",
                                                    compact('employees', 'totalPages', 'page')),
            ];
        }

        return $this->json($response);
    }

    /**
     * Ajoute un salarié
     * @param Request $request
     * @return Response
     */
    #[Route('/ajout', name: 'admin_employee_add')]
    public function add(Request $request): Response
    {
        $employee = new User();
        $employee->setPassword($this->userPasswordHasher->hashPassword($employee, "password"));
        $form = $this->createForm(EmployeeType::class, $employee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $employee->setRoles([Globals::ROLE_EMPLOYEE]);

            $photo = $form->get('photoToUpload')->getData();
            if ($photo) {
                $photoName = $this->uploadPhotoEmployee->upload($photo);
                $employee->setPhoto($photoName);
            }

            $this->employeeRepository->add($employee, true);

            return $this->redirectToRoute('admin_employee_index');
        }

        return $this->renderForm("backoffice/employee/add.html.twig", compact('form'));
    }

    /**
     * Edite un salarié et permet de mettre à jour
     * @param Request $request
     * @param User $employee
     * @return Response
     */
    #[Route('/{id}/editer', name: 'admin_employee_edit')]
    public function edit(Request $request, User $employee): Response
    {
        $form = $this->createForm(EmployeeType::class, $employee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $photo = $form->get('photoToUpload')->getData();
            if ($photo) {
                if ($employee->getPhoto()) {
                    unlink('uploads/employees/' . $employee->getPhoto());
                }
                $photoName = $this->uploadPhotoEmployee->upload($photo);
                $employee->setPhoto($photoName);
            }

            $this->entityManager->flush();

            return $this->redirectToRoute('admin_employee_index');
        }

        return $this->renderForm("backoffice/employee/edit.html.twig", compact('form'));
    }

    /**
     * Supprime un salarié
     * @param User $employee
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    #[Route('/{id}/supprimer', name: 'admin_employee_delete')]
    public function delete(User $employee)
    {
        $this->employeeRepository->remove($employee, true);

        return $this->redirectToRoute('admin_employee_index');
    }
}
