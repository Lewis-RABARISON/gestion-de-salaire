<?php

namespace App\Twig;

use App\Repository\WageRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class FormatMonthExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('formatMonth', [$this, 'formatMonth']),
            new TwigFunction('allMonth', [$this, 'getAllMonth']),
        ];
    }

    /**
     * Affiche le mois en lettre
     * @param string $month
     * @return string
     */
    public static function formatMonth(string $month): string
    {
        $month = match($month) {
        'Jan' => 'Janvier',
            'Feb' => 'Février',
            'Mar' => 'Mars',
            'Apr' => 'Avril',
            'May' => 'Mai',
            'Jun' => 'Juin',
            'Jul' => 'Juillet',
            'Aug' => 'Aout',
            'Sep' => 'Septembre',
            'Oct' => 'Octobre',
            'Nov' => 'Novembre',
            'Dec' => 'Decembre',
        };

        return $month;
    }

    /**
     * Les mois dans le filtre
     * @return array
     */
    public function getAllMonth(): array
    {
        $month_in_number = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];
        $month_in_letter = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'];
        $monthsOrdered = array_combine($month_in_number, $month_in_letter);

        return $monthsOrdered;
    }
}
