<?php

declare(strict_types=1);

namespace App\Utils;

class Globals
{
    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const ROLE_EMPLOYEE = 'ROLE_EMPLOYEE';
}