<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Utils\Globals;

class EmployeeFixtures extends Fixture implements DependentFixtureInterface
{
    public const PLAIN_PASSWORD = 'password';

    public function __construct(private UserPasswordHasherInterface $userPasswordHasher,
                                private UserRepository $userRepository)
    {
    }

    /**
     * Charge les fausse données
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        // Charge l'administrateur
        $this->loadAdmin($faker, $manager);

        // Charge employe pas aleatoire
        $this->loadEmployeeNotRandom($faker, $manager);

        // Charge une liste de 30 salariés
        $this->loadEmployees($faker, $manager);
    }


    /**
     * Charge les données de l'administrateur
     * @param $faker
     * @param ObjectManager $manager
     */
    public function loadAdmin($faker, ObjectManager $manager): void
    {
        $admin = new User();
        $faker = Factory::create();
        $admin->setEmail("rabarison.lewis@yahoo.com");
        $admin->setRoles([Globals::ROLE_ADMIN]);
        $admin->setPassword($this->userPasswordHasher->hashPassword($admin, self::PLAIN_PASSWORD));
        $admin->setContractType($this->getReference("contract_type_" . $faker->numberBetween(1, 2)));
        $admin->setMatricule("SAL".(string) rand(1000, 9000));
        $admin->setFirstname("Lewis");
        $admin->setLastname("RABARISON");
        $admin->setHiringDateAt(new \DateTimeImmutable());
        $admin->setDateOfBirthAt($faker->dateTimeBetween('-7 years', '-4 years'));
        $admin->setPhoneNumber($faker->phoneNumber());
        $admin->setWageAmount(rand(100000, 900000));

        $this->userRepository->add($admin);
    }

    public function loadEmployeeNotRandom($faker, ObjectManager $manager): void
    {
        $admin = new User();
        $faker = Factory::create();
        $admin->setEmail("jean.dupont@yahoo.com");
        $admin->setRoles([Globals::ROLE_EMPLOYEE]);
        $admin->setPassword($this->userPasswordHasher->hashPassword($admin, self::PLAIN_PASSWORD));
        $admin->setContractType($this->getReference("contract_type_" . $faker->numberBetween(1, 2)));
        $admin->setMatricule("SAL".(string) rand(1000, 9000));
        $admin->setFirstname("Jean");
        $admin->setLastname("Dupont");
        $admin->setHiringDateAt(new \DateTimeImmutable());
        $admin->setDateOfBirthAt($faker->dateTimeBetween('-7 years', '-4 years'));
        $admin->setPhoneNumber($faker->phoneNumber());
        $admin->setWageAmount(rand(100000, 900000));

        $this->userRepository->add($admin);
    }

    /**
     * Charge une liste de 29 salariés
     * @param $faker
     * @param ObjectManager $manager
     */
    public function loadEmployees($faker, ObjectManager $manager): void
    {
        for ($i = 1; $i < 30; $i++) {
            $employees = new User();
            $employees->setEmail($faker->email());
            $employees->setRoles([Globals::ROLE_EMPLOYEE]);
            $employees->setPassword($this->userPasswordHasher->hashPassword($employees, self::PLAIN_PASSWORD));
            $employees->setContractType($this->getReference("contract_type_" . $faker->numberBetween(0, 2)));
            $employees->setMatricule("SAL" . $i);
            $employees->setFirstname($faker->firstName());
            $employees->setLastname($faker->lastName());
            $employees->setHiringDateAt(new \DateTimeImmutable());
            $employees->setDateOfBirthAt(new \DateTime());
            $employees->setPhoneNumber($faker->phoneNumber());
            $employees->setWageAmount(rand(100000, 900000));
            $manager->persist($employees);
            $manager->flush();
            $this->addReference("employee_$i", $employees);
        }
    }

    /**
     * Recupère les dépendences sur le type de contrat
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [
            ContractTypeFixtures::class
        ];
    }
}
