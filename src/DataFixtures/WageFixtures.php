<?php

namespace App\DataFixtures;

use App\Entity\Wage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class WageFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * Charge une liste de 9 salaires
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        for ($i = 1; $i < 10; $i++) {
            $month = rand(1, 12);
            $day = rand(1, 28);
            $now = new \DateTime('now');
            $wage = new Wage();
            $wage->setEmployee($this->getReference("employee_$i"));
            $wage->setReceptionDate($now->modify("+ ". $month ." month")->modify("+ ". $day ." day"));
            $wage->setAmount(rand(100000, 900000));
            $manager->persist($wage);
        }

        $manager->flush();
    }

    /**
     * Recupère les dépendences sur les salariés
     * @return string[]
     */
    public function getDependencies()
    {
        return [
            EmployeeFixtures::class
        ];
    }
}
