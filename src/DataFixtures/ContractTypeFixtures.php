<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\ContractType;
use App\Repository\ContractTypeRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ContractTypeFixtures extends Fixture
{
    public const CONTRACT_TYPES = ['CDI', 'CDD', 'Freelance'];

    /**
     * Constructeur ContractTypeFixtures.
     * @param ContractTypeRepository $contractTypeRepository
     */
    public function __construct(private ContractTypeRepository $contractTypeRepository)
    {
    }

    /**
     * Charge les fausses données du type de contrat
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < count(self::CONTRACT_TYPES); $i++) {
            $contractTypes = new ContractType();
            $contractTypes->setLabel(self::CONTRACT_TYPES[$i]);
            $this->contractTypeRepository->add($contractTypes);
            $this->addReference("contract_type_$i", $contractTypes);
        }
    }
}
