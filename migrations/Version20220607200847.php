<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220607200847 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user ADD contract_type_id INT NOT NULL, ADD matricule VARCHAR(15) NOT NULL, ADD firstname VARCHAR(70) DEFAULT NULL, ADD lastname VARCHAR(60) NOT NULL, ADD hiring_date_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD date_of_birth_at DATETIME NOT NULL, ADD phone_number VARCHAR(20) DEFAULT NULL, ADD photo VARCHAR(255) DEFAULT NULL, ADD wage DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649CD1DF15B FOREIGN KEY (contract_type_id) REFERENCES contract_type (id)');
        $this->addSql('CREATE INDEX IDX_8D93D649CD1DF15B ON user (contract_type_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649CD1DF15B');
        $this->addSql('DROP INDEX IDX_8D93D649CD1DF15B ON user');
        $this->addSql('ALTER TABLE user DROP contract_type_id, DROP matricule, DROP firstname, DROP lastname, DROP hiring_date_at, DROP date_of_birth_at, DROP phone_number, DROP photo, DROP wage');
    }
}
