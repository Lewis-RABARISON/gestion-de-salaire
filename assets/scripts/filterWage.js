const $ = require('jquery');

$(document).ready(function () {
    filterDataWage();
});

/**
 * Filtre la liste des salaires en fonction du salarié, mois et l'année
 */
function filterDataWage() {
    $("#filter-wage").on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: "json",
            beforeSend: function () {
                $(".content").html(` <tr align="center">
                    <td colspan="10">
                    <div class="spinner-border spinner-border-detail-materiel text-primary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </td>
                </tr>`);
            },
            success: function (response) {
                if (response.success) {
                    $(".content").html(response.wages);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    });
}