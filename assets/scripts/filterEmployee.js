const $ = require('jquery');

$(document).ready(function () {
    filterDataEmployee();
});

/**
 * Filtre la liste des salariés en fonction de matricule, nom, prenom
 */
function filterDataEmployee() {
    $("#filter-employee").on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: "json",
            beforeSend: function () {
                $(".content").html(` <tr align="center">
                    <td colspan="10">
                    <div class="spinner-border spinner-border-detail-materiel text-primary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </td>
                </tr>`);
            },
            success: function (response) {
                if (response.success) {
                    $(".content").html(response.employees);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    });
}