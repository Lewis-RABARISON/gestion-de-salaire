import './styles/app.scss';
require('@fortawesome/fontawesome-free/css/all.min.css');
const $ = require('jquery');
import 'bootstrap'
import '../assets/plugins/jquery-easing/jquery.easing.min';
import './scripts/sb-admin-2.min';
import 'select2'
const routes = require('../public/js/fos_js_routes.json');
import Routing from '../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min';
Routing.setRoutingData(routes);

$(document).ready(function(){
    tooltipAction();
    paginationAjax();
    selectTwo();
});

/**
 * Affiche un tooltip sur l'action editer et supprimer
 */
function tooltipAction() {
    $('[data-toggle="tooltip"]').tooltip();
}

/**
 * Pagination en ajax
 */
function paginationAjax(){
    $('.page-item').click(function (e) {
        e.preventDefault();
        let page = $(this).attr('data-id');
        let url = Routing.generate('admin_employee_index');
        $.ajax({
            type: "POST",
            url: url,
            data: {page:page},
            dataType: "json",
            beforeSend: function () {
                $(".content").html(` <tr align="center">
                    <td colspan="10">
                    <div class="spinner-border spinner-border-detail-materiel text-primary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </td>
                </tr>`);
            },
            success: function (response) {
                if (response.success) {
                    $(".content").html(response.employees);
                    $(".pagination-customized").html(response.pagination);
                    console.log(response.pagination);
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    });
}

/**
 * Permet de faire un recherche sur la liste déroulante
 */
function selectTwo() {
    $('.selectTwo').select2();
}



