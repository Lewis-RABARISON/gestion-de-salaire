##Exigences
- Php 8
- Node.js (Pour avoir le gestionnaire de paquet npm)
- Composer (Gestionnaire de dependance de php)

## Etapes
```
- Renommer le fichier .your-env en .env
- Changer le db_user par votre nom d'utilisateur, le db_password par votre mot de passe et db_name par le nom de votre base de donnée
```

## Installation
```
- Front: npm install et npm run build
- Back: composer install
```

## Donnee 
```
- Pour avoir des données disponibles, lancer la commande : composer prepare-database
```

## Utilisateur (Admin)
```
- email: rabarison.lewis@yahoo.com
- mot de passe: password
```

## Utilisateur (Employée)
```
- email: jean.dupont@yahoo.com
- mot de passe: password
```

## Lancement de l'application
* Si vous avez le cli de symfony
```
- Lancer la commande: symfony serve -d 
```

* Si vous n'avez pas le cli de symfony
```
- Lancer la commande: php -S 127.0.0.1:8000 -t public
```

## Importation du fichier csv
```
- Lancer cette commande: php bin/console app:import-data-wage-csv nom_fichier_csv_exporte
```
